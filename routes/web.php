<?php

use App\Models\House;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $houses = House::with('status')->paginate(8);

    return view('welcome',compact('houses'));
});



// dashboard routes
Route::group(['middleware' => 'auth', 'prefix' => 'dashboard'], function() {

    Route::get('/', function(){
        return view('dashboard');
    })->name('dashboard');

    Route::resource('houses', 'HouseController');


});


require __DIR__.'/auth.php';
