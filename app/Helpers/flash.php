<?php
namespace App\Helpers;

class Flash{


    public static function info(string $string){

        session()->put('message.type', 'bg-blue-500');
        session()->flash('message.content', $string);

    }

    public static function warning(string $string){

        session()->put('message.type', 'bg-red-500');
        session()->flash('message.content', $string);

    }

    public static function success(string $string){

        session()->put('message.type', 'bg-green-500');
        session()->flash('message.content', $string);

    }

}