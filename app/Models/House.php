<?php

namespace App\Models;

use GuzzleHttp\Psr7\UploadedFile;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;
//$table->string('name');
//$table->string('type');
//
//$table->integer('square_meters');
//$table->integer('rooms_amount');
//
//$table->string('price');
//$table->foreignId('status_id');
    protected $fillable = [
        'name',
        'type',
        'square_meters',
        'rooms_amount',
        'price',
        'status_id'
    ];

    protected $casts = [
        'square_meters' => 'string',
        'type' => 'string',
        'rooms_amount' => 'integer',
        'price' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status(){
        return $this->belongsTo(Status::class);
    }

    public function storeImage($file){

        $fileName = $file->getClientOriginalName();

        return $file->storeAs('/', $fileName, 'uploads');

    }

    public function sold(){

        if($this->status_id == 4){
            return true;
        }

        return false;

    }


}
