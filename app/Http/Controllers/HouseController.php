<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHouseRequest;
use App\Models\House;
use App\Models\Status;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;

use App\Helpers\Flash;
/**
 * Class HouseController
 * @package App\Http\Controllers
 */
class HouseController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){

        $houses = House::with('status')->paginate(8);

        return view('house.index', compact('houses'));

    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(){

        $statuses = Status::all();

        return view('house.create', compact('statuses'));

    }

    /**
     * @param StoreHouseRequest $storeHouseRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreHouseRequest $storeHouseRequest){

        $house = House::create($storeHouseRequest->except(['house_image']));

        if($storeHouseRequest->hasFile('house_image')){
            $house->image_uri = $house->storeImage($storeHouseRequest->file('house_image'));
            $house->save();
        }
        
        Flash::success('Het huis is toegevoegd!');

        return redirect()->route('houses.index');


    }

    /**
     * @param House $house
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(House $house){

        if(!$house->delete()){
            abort('400', 'Er ging iets mis.');
        }

        if($house->image_uri){
            Storage::disk('uploads')->delete($house->image_uri);
        }
        
        Flash::success('Het huis is verwijderd uit de database!');
        
        return redirect()->back();

    }

    /**
     * @param House $house
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(House $house){

        $statuses = Status::all();

        return view('house.edit', compact('house','statuses'));

    }


    /**
     * @param StoreHouseRequest $storeHouseRequest
     * @param House $house
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreHouseRequest $storeHouseRequest, House $house){

        $house->update($storeHouseRequest->validated());

        if( $storeHouseRequest->has('house_image') ){
            $house->image_uri = $house->storeImage($storeHouseRequest->file('house_image'));
            $house->save();
        }

        Flash::success('Huis: ' . $house->name . ' is geupdated!');

        return redirect()->back();


    }

}
