<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreHouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required',
            'square_meters' => 'required',
            'rooms_amount' => 'required',
            'price' => 'required',
            'status_id' => 'required',
            'house_image' => 'mimes:png,jpg,jpeg|max:10000'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Het veld naam is vereist',
            'type.required' => 'Selecteer a.u.b het type woning',
            'square_meters.required' => 'Vul het aantal vierkante meters in',
            'rooms_amount.required' => 'Vul het aantal kamers in',
            'price.required' => 'Vul a.u.b een prijs in',
            'status_id.required' => 'Vul een status in',
            'house_image.mimes' => 'Alleen png,jpg en jpeg zijn toegelaten'
        ];
    }

    protected function prepareForValidation(){
            $this->merge([
                'status_id' => (int)$this->status_id
            ]);
    }
}
