<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1 class="text-5xl mb-10">Huizen</h1>
                    <table class="min-w-max w-full table-auto">
                        <thead>
                            <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                <th class="py-3 px-6 text-left">Naam</th>
                                <th class="py-3 px-6 text-left">Prijs</th>
                                <th class="py-3 px-6 text-left">Status</th>
                                <th class="py-3 px-6 text-left">Acties</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($houses as $house)
                                <tr class="border-b border-gray-200 hover:bg-gray-100">
                                    <td class="py-3 px-6 text-left">{{$house->name}}</td>
                                    <td class="py-3 px-6 text-center">{{$house->price}}</td>
                                    <td class="py-3 px-6 text-center">{{$house->status->name}}</td>
                                    <td class="py-3 px-6 text-center flex justify-end">
                                        <form action="{{route('houses.destroy', $house->id)}}" method="POST">
                                            @method('delete')
                                            @csrf
                                            <input type="submit" onclick="confirm('Weet je het zeker dat je huis:{{$house->name}} wilt verwijderen?')" class="p-2 bg-red-700 text-white cursor-pointer" value="verwijderen">
                                        </form>
                                        <a href="{{route('houses.edit', $house->id)}}" class="p-2 bg-green-600 text-white ml-1">wijzigen</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="my-10">
                        {{$houses->links()}}

                    </div>
                    <div class="mt-10">
                        <a href="{{route('houses.create')}}" class="mt-5 p-2 bg-blue-700 text-white">Nieuw huis toevoegen</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
