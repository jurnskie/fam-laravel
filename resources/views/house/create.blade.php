<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1 class="text-5xl mb-10">Een nieuw huis toevoegen</h1>
                    <form action="{{route('houses.store')}}" method="POST">
                        @csrf
                        <div class="field">
                            <input type="text" name="name" placeholder="Naam">
                        </div>
                        <div class="field mt-2">
                            <select name="type" id="type">
                                <option value="">Selecteer Type</option>
                                <option value="rijtjeshuis">Rijtjeshuis</option>
                                <option value="rijtjeshuis">Appartement</option>
                                <option value="rijtjeshuis">Vrijstaand</option>
                            </select>
                        </div>
                        <div class="field mt-2">
                            <input type="number" name="square_meters" placeholder="m2">
                        </div>
                        <div class="field mt-2">
                            <input type="number" name="rooms_amount" placeholder="Aantal kamers">
                        </div>
                        <div class="field mt-2">
                            <input type="number" name="price" placeholder="Prijs">
                        </div>
                        <div class="field mt-2">
                            <select name="status_id" id="status_id">
                                @foreach($statuses as $status)
                                    <option value="{{$status->id}}">{{$status->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field mt-2">
                            <label for="house_image">Upload een foto</label><br>
                            <input type="file" name="house_image">
                        </div>
                        <div class="mt-10">
                            <input type="submit" class="mt-5 p-2 bg-blue-700 text-white" value="Huis toevoegen" formenctype="multipart/form-data"/>
                        </div>
                    </form>
                    @if ($errors->any())
                        <div class="alert alert-danger bg-red-700 text-white mt-5">
                            <ul class="p-10">
                                @foreach ($errors->all() as $error)
                                    <li class="mt-5">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
