<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1 class="text-5xl mb-10">Huis: <strong>{{$house->name}}</strong> bewerken</h1>
                    @if($house->image_uri)
                        <img src="{{asset('/uploads/'.$house->image_uri)}}" alt="jaja" >
                    @endif
                    <form action="{{route('houses.update', $house->id)}}" method="POST">
                        @csrf
                        @method('put')
                        <div class="field">
                            <input type="text" name="name" placeholder="Naam" value="{{$house->name}}">
                        </div>
                        <div class="field mt-2">
                            <select name="type" id="type">
                                <option value="rijtjeshuis"
                                    @if($house->type == 'rijtjeshuis')
                                        selected
                                    @endif
                                >Rijtjeshuis</option>
                                <option value="appartement"
                                    @if($house->type == 'appartement')
                                        selected
                                    @endif
                                >Appartement</option>
                                <option value="vrijstaand"
                                    @if($house->type == 'vrijstaand')
                                        selected
                                    @endif
                                >Vrijstaand</option>
                            </select>
                        </div>
                        <div class="field mt-2">
                            <input type="number" name="square_meters" placeholder="Vierkante meters" value="{{$house->square_meters}}">
                        </div>
                        <div class="field mt-2">
                            <input type="number" name="rooms_amount" placeholder="Aantal kamers" value="{{$house->rooms_amount}}">
                        </div>
                        <div class="field mt-2">
                            <input type="number" name="price" placeholder="Prijs" value="{{$house->price}}">
                        </div>
                        <div class="field mt-2">
                            <select name="status_id" id="status_id">
                                @foreach($statuses as $status)
                                    <option value="{{$status->id}}"
                                        @if($status->id == $house->status_id)
                                            selected
                                        @endif
                                    >{{$status->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field mt-2">
                            <label for="house_image">Wijzig afbeelding</label><br>
                            <input type="file" name="house_image">
                        </div>
                        <div class="mt-10">
                            <input type="submit" class="mt-5 p-2 bg-blue-700 text-white" value="Huis updaten" formenctype="multipart/form-data"/>
                        </div>
                    </form>
                    @if ($errors->any())
                        <div class="alert alert-danger bg-red-700 text-white mt-5">
                            <ul class="p-10">
                                @foreach ($errors->all() as $error)
                                    <li class="mt-5">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
