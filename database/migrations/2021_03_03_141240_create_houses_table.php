<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('type');

            $table->integer('square_meters');
            $table->integer('rooms_amount')->default('3');

            $table->string('price')->default('0');
            $table->foreignId('status_id')->default(1);
            
            $table->string('image_uri')->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house');
    }
}
