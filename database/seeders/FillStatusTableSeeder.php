<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class FillStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = collect([
           'in_verkoop',
           'in optie',
           'verkocht onder voorbehoud',
           'verkocht'
        ]);

        foreach ($statuses as $status){
            Status::create(['name' => $status]);
        }
    }
}
