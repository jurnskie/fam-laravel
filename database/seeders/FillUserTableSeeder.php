<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class FillUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'FAM',
            'email' => 'demo@fam.nl',
            'password' => Hash::make(env('ADMIN_PASSWORD'))
        ]);
    }
}
